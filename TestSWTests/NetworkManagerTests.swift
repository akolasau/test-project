//
//  NetworkManagerTests.swift
//  TestSWTests
//
//  Created by Anton on 27/03/24.
//

import XCTest
@testable import TestSW
import Combine

final class NetworkManagerTests: XCTestCase {
    
    private var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        cancellables = []
    }

    override func tearDownWithError() throws {
        cancellables = nil
    }

    func testFetchPeople() throws {
        let networkManager = NetworkManager()
        var receivedValue: Any? = nil
        var error: Error?
        let expectation = self.expectation(description: "Fetch Events")
        
        networkManager.fetchPeople()
            .sink { status in
                switch status {
                case .failure(let encounteredError):
                    error = encounteredError
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { value in
                receivedValue = value
            }
            .store(in: &cancellables)
        waitForExpectations(timeout: 5)
        XCTAssertNil(error)
        XCTAssertNotNil(receivedValue as? [Person])
    }
    
    func testFetchHomeworld_InvalidInput() throws {
        let networkManager = NetworkManager()
        var error: Error?
        let expectation = self.expectation(description: "Fetch Events")
        
        networkManager.fetchPersonHomeworld(personID: "-12")
            .sink { status in
                switch status {
                case .failure(let encounteredError):
                    error = encounteredError
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { _ in
            }
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 5)
        XCTAssertNotNil(error)
    }
    
    func testFetchHomeworld() {
        let testID = "cGVvcGxlOjE="
        let networkManager = NetworkManager()
        var receivedValue: Any? = nil
        var error: Error?
        let expectation = self.expectation(description: "Fetch Events")
        
        networkManager.fetchPersonHomeworld(personID: testID)
            .sink { status in
                switch status {
                case .failure(let encounteredError):
                    error = encounteredError
                case .finished:
                    break
                }
                expectation.fulfill()
            } receiveValue: { value in
                receivedValue = value
            }
            .store(in: &cancellables)
        
        waitForExpectations(timeout: 5)
        XCTAssertNil(error)
        XCTAssertNotNil(receivedValue as? Homeworld)
    }
}
