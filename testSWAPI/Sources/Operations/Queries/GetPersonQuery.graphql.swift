// @generated
// This file was automatically generated and should not be edited.

@_exported import ApolloAPI

public class GetPersonQuery: GraphQLQuery {
  public static let operationName: String = "GetPersonQuery"
  public static let operationDocument: ApolloAPI.OperationDocument = .init(
    definition: .init(
      #"query GetPersonQuery($id: ID!) { person(id: $id) { __typename homeworld { __typename name } } }"#
    ))

  public var id: ID

  public init(id: ID) {
    self.id = id
  }

  public var __variables: Variables? { ["id": id] }

  public struct Data: TestSWAPI.SelectionSet {
    public let __data: DataDict
    public init(_dataDict: DataDict) { __data = _dataDict }

    public static var __parentType: ApolloAPI.ParentType { TestSWAPI.Objects.Root }
    public static var __selections: [ApolloAPI.Selection] { [
      .field("person", Person?.self, arguments: ["id": .variable("id")]),
    ] }

    public var person: Person? { __data["person"] }

    /// Person
    ///
    /// Parent Type: `Person`
    public struct Person: TestSWAPI.SelectionSet {
      public let __data: DataDict
      public init(_dataDict: DataDict) { __data = _dataDict }

      public static var __parentType: ApolloAPI.ParentType { TestSWAPI.Objects.Person }
      public static var __selections: [ApolloAPI.Selection] { [
        .field("__typename", String.self),
        .field("homeworld", Homeworld?.self),
      ] }

      /// A planet that this person was born on or inhabits.
      public var homeworld: Homeworld? { __data["homeworld"] }

      /// Person.Homeworld
      ///
      /// Parent Type: `Planet`
      public struct Homeworld: TestSWAPI.SelectionSet {
        public let __data: DataDict
        public init(_dataDict: DataDict) { __data = _dataDict }

        public static var __parentType: ApolloAPI.ParentType { TestSWAPI.Objects.Planet }
        public static var __selections: [ApolloAPI.Selection] { [
          .field("__typename", String.self),
          .field("name", String?.self),
        ] }

        /// The name of this planet.
        public var name: String? { __data["name"] }
      }
    }
  }
}
