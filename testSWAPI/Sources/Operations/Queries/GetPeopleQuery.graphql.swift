// @generated
// This file was automatically generated and should not be edited.

@_exported import ApolloAPI

public class GetPeopleQuery: GraphQLQuery {
  public static let operationName: String = "GetPeopleQuery"
  public static let operationDocument: ApolloAPI.OperationDocument = .init(
    definition: .init(
      #"query GetPeopleQuery { allPeople { __typename people { __typename id name mass height } totalCount } }"#
    ))

  public init() {}

  public struct Data: TestSWAPI.SelectionSet {
    public let __data: DataDict
    public init(_dataDict: DataDict) { __data = _dataDict }

    public static var __parentType: ApolloAPI.ParentType { TestSWAPI.Objects.Root }
    public static var __selections: [ApolloAPI.Selection] { [
      .field("allPeople", AllPeople?.self),
    ] }

    public var allPeople: AllPeople? { __data["allPeople"] }

    /// AllPeople
    ///
    /// Parent Type: `PeopleConnection`
    public struct AllPeople: TestSWAPI.SelectionSet {
      public let __data: DataDict
      public init(_dataDict: DataDict) { __data = _dataDict }

      public static var __parentType: ApolloAPI.ParentType { TestSWAPI.Objects.PeopleConnection }
      public static var __selections: [ApolloAPI.Selection] { [
        .field("__typename", String.self),
        .field("people", [Person?]?.self),
        .field("totalCount", Int?.self),
      ] }

      /// A list of all of the objects returned in the connection. This is a convenience
      /// field provided for quickly exploring the API; rather than querying for
      /// "{ edges { node } }" when no edge data is needed, this field can be be used
      /// instead. Note that when clients like Relay need to fetch the "cursor" field on
      /// the edge to enable efficient pagination, this shortcut cannot be used, and the
      /// full "{ edges { node } }" version should be used instead.
      public var people: [Person?]? { __data["people"] }
      /// A count of the total number of objects in this connection, ignoring pagination.
      /// This allows a client to fetch the first five objects by passing "5" as the
      /// argument to "first", then fetch the total count so it could display "5 of 83",
      /// for example.
      public var totalCount: Int? { __data["totalCount"] }

      /// AllPeople.Person
      ///
      /// Parent Type: `Person`
      public struct Person: TestSWAPI.SelectionSet {
        public let __data: DataDict
        public init(_dataDict: DataDict) { __data = _dataDict }

        public static var __parentType: ApolloAPI.ParentType { TestSWAPI.Objects.Person }
        public static var __selections: [ApolloAPI.Selection] { [
          .field("__typename", String.self),
          .field("id", TestSWAPI.ID.self),
          .field("name", String?.self),
          .field("mass", Double?.self),
          .field("height", Int?.self),
        ] }

        /// The ID of an object
        public var id: TestSWAPI.ID { __data["id"] }
        /// The name of this person.
        public var name: String? { __data["name"] }
        /// The mass of the person in kilograms.
        public var mass: Double? { __data["mass"] }
        /// The height of the person in centimeters.
        public var height: Int? { __data["height"] }
      }
    }
  }
}
