// @generated
// This file was automatically generated and should not be edited.

import ApolloAPI

public typealias ID = String

public protocol SelectionSet: ApolloAPI.SelectionSet & ApolloAPI.RootSelectionSet
where Schema == TestSWAPI.SchemaMetadata {}

public protocol InlineFragment: ApolloAPI.SelectionSet & ApolloAPI.InlineFragment
where Schema == TestSWAPI.SchemaMetadata {}

public protocol MutableSelectionSet: ApolloAPI.MutableRootSelectionSet
where Schema == TestSWAPI.SchemaMetadata {}

public protocol MutableInlineFragment: ApolloAPI.MutableSelectionSet & ApolloAPI.InlineFragment
where Schema == TestSWAPI.SchemaMetadata {}

public enum SchemaMetadata: ApolloAPI.SchemaMetadata {
  public static let configuration: ApolloAPI.SchemaConfiguration.Type = SchemaConfiguration.self

  public static func objectType(forTypename typename: String) -> ApolloAPI.Object? {
    switch typename {
    case "Root": return TestSWAPI.Objects.Root
    case "Person": return TestSWAPI.Objects.Person
    case "Film": return TestSWAPI.Objects.Film
    case "Planet": return TestSWAPI.Objects.Planet
    case "Species": return TestSWAPI.Objects.Species
    case "Starship": return TestSWAPI.Objects.Starship
    case "Vehicle": return TestSWAPI.Objects.Vehicle
    case "PeopleConnection": return TestSWAPI.Objects.PeopleConnection
    default: return nil
    }
  }
}

public enum Objects {}
public enum Interfaces {}
public enum Unions {}
