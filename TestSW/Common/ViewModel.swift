//
//  ViewModel.swift
//  TestSW
//
//  Created by Anton on 27/03/24.
//

import Foundation
import Combine

class ViewModel: ObservableObject {
    var cancellable = Set<AnyCancellable>()
}
