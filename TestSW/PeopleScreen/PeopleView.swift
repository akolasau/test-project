//
//  ContentView.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import SwiftUI
import NavigationTransitions
import TestSWAPI

struct PeopleView: View {
    @StateObject var viewModel: PeopleViewModel
    
    var body: some View {
        ZStack {
            BackgroundView()
            
            switch viewModel.state {
            case .loading:
                LoadingView()
            case .failed(let error):
                ErrorView(title: error.localizedDescription) {
                    viewModel.fetchPeople()
                }
            case .loaded(let people):
                NavigationStack {
                    VStack {
                        Text("People")
                            .foregroundStyle(Color.white)
                        List(people, id: \.self.id) { person in
                            NavigationLink(value: person) {
                                PeopleItemView(title: person.name ?? "", mass: person.mass ?? 0, height: person.height ?? 0)
                            }
                            .listRowBackground(Color.clear)
                            .listRowSeparatorTint(Color.white)
                            .foregroundColor(Color.white)
                        }
                        .scrollContentBackground(.hidden)
                        .refreshable {
                            viewModel.fetchPeople()
                        }
                    }
                    .background(BackgroundView())
                    .navigationDestination(for: Person.self) { person in
                        PersonView(viewModel: PersonViewModel(person: person, networkManager: viewModel.networkManager))
                    }
                }
                .navigationTransition(
                    .fade(.cross).combined(with: .slide)
                    .animation(.easeIn(duration: 0.4))
                )
                .tint(Color.white)
            }
        }
        .onAppear(perform: {
            viewModel.fetchPeople()
        })
    }
}
