//
//  PeopleItemTextView.swift
//  TestSW
//
//  Created by Anton on 27/03/24.
//

import SwiftUI

struct PeopleItemTextView: View {
    let title: String
    var body: some View {
        Text(title)
            .foregroundStyle(.white)
            .multilineTextAlignment(.leading)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
}
