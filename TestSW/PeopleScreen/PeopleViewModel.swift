//
//  PeopleViewModel.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import Foundation
import Combine
import TestSWAPI

class PeopleViewModel: ViewModel {
    let networkManager: NetworkManagerProtocol
    private var people: [Person] = []
    
    enum State {
        case loading
        case failed(Error)
        case loaded([Person])
    }
    
    @Published private(set) var state = State.loading
    
    init(people: [Person], networkManager: NetworkManagerProtocol) {
        self.people = people
        self.networkManager = networkManager
    }
    
    func fetchPeople() {
        self.state = .loading
        self.networkManager.fetchPeople()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] status in
                switch status {
                case .finished:
                    break
                case .failure(let error):
                    self?.state = .failed(error)
                    break
                } }, receiveValue: { [weak self] data in
                    self?.people = data
                    self?.state = .loaded(data)
                })
            .store(in: &self.cancellable)
    }
}
