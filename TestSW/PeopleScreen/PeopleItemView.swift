//
//  PeopleItemView.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import SwiftUI

struct PeopleItemView: View {
    let title: String
    let mass: Double
    let height: Int
    var body: some View {
        VStack {
            PeopleItemTextView(title: title)
            PeopleItemTextView(title: "Height: \(height)")
            PeopleItemTextView(title: "Mass: \(String(format: "%.1f",mass))")
        }
    }
}

