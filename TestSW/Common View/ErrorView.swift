//
//  ErrorView.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import SwiftUI

struct ErrorView: View {
    let title: String
    let action: () -> Void
    
    var body: some View {
        Text(title)
        Button("Retry") {
            action()
        }
    }
}
