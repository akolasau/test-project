//
//  LoadingView.swift
//  TestSW
//
//  Created by Anton on 26/03/24.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        ProgressView() {
            Text("Loading...")
                .foregroundStyle(Color.white)
        }
        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
    }
}
