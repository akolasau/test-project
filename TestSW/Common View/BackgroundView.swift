//
//  BackgroundView.swift
//  TestSW
//
//  Created by Anton on 26/03/24.
//

import SwiftUI

struct BackgroundView: View {
    var body: some View {
        LinearGradient(colors: [Color("background-gradient-start", bundle: Bundle.main), Color("background-gradient-end", bundle: Bundle.main)], startPoint: .top, endPoint: .bottom).ignoresSafeArea()
    }
}
