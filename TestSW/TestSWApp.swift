//
//  TestSWApp.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import SwiftUI

@main
struct TestSWApp: App {
    var body: some Scene {
        WindowGroup {
            PeopleView(viewModel: PeopleViewModel(people: [], networkManager: NetworkManager()))
        }
    }
}
