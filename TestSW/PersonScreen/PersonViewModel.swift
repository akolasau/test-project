//
//  PersonViewModel.swift
//  TestSW
//
//  Created by Anton on 26/03/24.
//

import Foundation
import Combine
import TestSWAPI

class PersonViewModel: ViewModel {
    private let networkManager: NetworkManagerProtocol
    private let person: Person
    private var homeworld: Homeworld?
    
    @Published private(set) var state = State.loading
    
    enum State {
        case loading
        case failed(Error)
        case loaded(String, String)
    }
    
    init(person: Person, networkManager: NetworkManagerProtocol) {
        self.person = person
        self.networkManager = networkManager
    }
    
    func fetchHomeworld() {
        self.state = .loading
        
        self.networkManager.fetchPersonHomeworld(personID: self.person.id)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] status in
                switch status {
                case .finished:
                    break
                case .failure(let error):
                    self?.state = .failed(error)
                    break
                } }, receiveValue: { [weak self] data in
                    self?.homeworld = data
                    let personName = self?.person.name ?? ""
                    let homeworldName = self?.homeworld?.name ?? ""
                    self?.state = .loaded(personName, homeworldName)
                })
            .store(in: &self.cancellable)
    }
    
    func descriptionText() -> AttributedString {
        guard let name = person.name else { return AttributedString("") }
        var here = AttributedString("here")
        here.foregroundColor = .blue
        here.link = URL(string: "testSW://opendetails")
        return AttributedString("Click ") + here + AttributedString(" to view the homeworld data for \(name)")
    }
}
