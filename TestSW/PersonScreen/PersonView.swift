//
//  PersonView.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import SwiftUI

struct PersonView: View {
    @StateObject var viewModel: PersonViewModel
    @State private var isHomeworldDetailPresented = false
    
    var body: some View {
        ZStack {
            BackgroundView()
            
            switch viewModel.state {
            case .loading:
                LoadingView()
            case .failed(let error):
                ErrorView(title: error.localizedDescription) {
                    viewModel.fetchHomeworld()
                }
            case .loaded(let personName, let homeworldName):
                VStack {
                    Text(viewModel.descriptionText())
                        .foregroundStyle(Color.white)
                        .onOpenURL(perform: { url in
                            withAnimation {
                                isHomeworldDetailPresented = true
                            }
                        })
                        .frame(alignment: .topLeading)
                    Spacer()
                }
                if isHomeworldDetailPresented {
                    HomeworldView(name: personName, homeworld: homeworldName)
                        .transition(.move(edge: .bottom))
                }
            }
        }
        .onTapGesture {
            if isHomeworldDetailPresented {
                withAnimation {
                    isHomeworldDetailPresented = false
                }
            }
        }
        .onAppear {
            viewModel.fetchHomeworld()
        }
    }
}
