//
//  HomeworldView.swift
//  TestSW
//
//  Created by Anton on 26/03/24.
//

import SwiftUI

struct HomeworldView: View {
    let name: String
    let homeworld: String
    
    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerSize: CGSize(width: 20, height: 20))
                .foregroundStyle(Color.white)
                .ignoresSafeArea(edges: [.bottom])
            VStack(alignment: .leading) {
                    Text("Name: \(name)")
                        .font(.system(size: 24))
                    Text("Homeworld: \(homeworld)")
                Spacer()
            }
            .foregroundStyle(.black)
            .padding()
        }
        .padding(.top, 500)
    }
}
