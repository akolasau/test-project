//
//  NetworkManagerProtocol.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import Foundation
import Combine
import TestSWAPI

protocol NetworkManagerProtocol {
    // method will fetch all people
    func fetchPeople() -> PassthroughSubject<[GetPeopleQuery.Data.AllPeople.Person], Error>
    // method will fetch specific homeworld by person ID
    func fetchPersonHomeworld(personID: String) -> PassthroughSubject<GetPersonQuery.Data.Person.Homeworld, Error>
}
