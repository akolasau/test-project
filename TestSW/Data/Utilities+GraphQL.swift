//
//  Utilities+GraphQL.swift
//  TestSW
//
//  Created by Anton on 26/03/24.
//

import Foundation
import TestSWAPI

typealias Person = GetPeopleQuery.Data.AllPeople.Person
typealias Homeworld = GetPersonQuery.Data.Person.Homeworld
