//
//  NetworkManager.swift
//  TestSW
//
//  Created by Anton on 25/03/24.
//

import Foundation
import Combine
import Apollo
import TestSWAPI

enum APIError: LocalizedError {
    case missingData
}

final class NetworkManager: NetworkManagerProtocol {
    private lazy var apolloClient: ApolloClient = {
        guard let url = URL(string: "https://swapi-graphql.netlify.app/.netlify/functions/index") else {
            fatalError("Unale to create apollo client")
        }
        let apollo = ApolloClient(url: url)
        return apollo
    }()
    
    func fetchPeople() -> PassthroughSubject<[Person], Error> {
        let subject = PassthroughSubject<[Person], Error>()
        apolloClient.fetch(query: GetPeopleQuery()) { result in
            switch result {
            case .success(let data):
                guard let people = data.data?.allPeople?.people?.compactMap({$0}) else {
                    subject.send(completion: .failure(APIError.missingData))
                    return
                }
                subject.send(people)
                subject.send(completion: .finished)
            case .failure(let error):
                subject.send(completion: .failure(error))
            }
        }
        return subject
    }
    
    func fetchPersonHomeworld(personID: String) -> PassthroughSubject<Homeworld, Error> {
        let subject = PassthroughSubject<Homeworld, Error>()
        apolloClient.fetch(query: GetPersonQuery(id: personID)) { result in
            switch result {
            case .success(let data):
                guard let homeworld = data.data?.person?.homeworld else {
                    subject.send(completion: .failure(APIError.missingData))
                    return
                }
                subject.send(homeworld)
                subject.send(completion: .finished)
            case .failure(let error):
                subject.send(completion: .failure(error))
            }
        }
        return subject
    }
}
