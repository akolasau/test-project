Project Goal:
The aim of this project is to implement 2 screens using SwiftUI to display data fetched from the GraphQL StarWars API.

Approach:
I adopted the MVVM (Model-View-ViewModel) approach due to the project's relatively small scale, eliminating the need for a complex architecture.

Navigation:
Default navigation link and navigation destination were employed for navigation purposes within the application.

Animated Navigation:
To enhance user experience, SwiftUI Navigation Transitions were utilized for animated navigation effects.